import java.io.Serializable;

public class Task implements Serializable {
    private String insert_un;
    private String task_title;
    private String task_description;

  Task(){};

    Task(String insert_un, String task_title, String task_description) {
        this.insert_un = insert_un;
        this.task_title = task_title;
        this.task_description = task_description;
    }

    public String getInsert_un() {
        return insert_un;
    }

    public void setInsert_un(String insert_un) {
        this.insert_un = insert_un;
    }

    public String getTask_title() {
        return task_title;
    }

    public void setTask_title(String task_title) {
        this.task_title = task_title;
    }

    public String getTask_description() {
        return task_description;
    }

    public void setTask_description(String task_description) {
        this.task_description = task_description;
    }

    @Override
    public String toString() {
        return "User_name: " + insert_un + " Task_title: " + task_title + " Task_description: " + task_description;
    }
}
