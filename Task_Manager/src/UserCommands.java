import java.io.*;
import java.util.ArrayList;

public class UserCommands implements Serializable {
    static ArrayList<User> userList = new ArrayList<>();
    static File f = new File("C:\\Users\\User\\IdeaProjects\\exercise2_taskmanager\\Task_Manager\\out\\artifacts\\Task_Manager_jar\\users");

    public static void createUser(String fname, String lname, String uname) throws IOException, ClassNotFoundException {
        User user = new User();
        user.setFirst_name(fname);
        user.setLast_name(lname);
        user.setUser_name(uname);
        if (f.length() == 0) {
            FileOutputStream fos = new FileOutputStream("C:\\Users\\User\\IdeaProjects\\exercise2_taskmanager\\Task_Manager\\out\\artifacts\\Task_Manager_jar\\users");
            MyObjectOutputStream oos = new MyObjectOutputStream(f);
            userList.add(user);
            oos.writeObject(userList);
            oos.reset();
            oos.close();
            fos.close();
        } else {
            FileInputStream fis = new FileInputStream("C:\\Users\\User\\IdeaProjects\\exercise2_taskmanager\\Task_Manager\\out\\artifacts\\Task_Manager_jar\\users");
            ObjectInputStream ois = new ObjectInputStream(fis);
            userList = (ArrayList<User>) ois.readObject();
            ois.close();
            fis.close();
            boolean unique = true;
            for (User oldUser : userList) {
                if (user.getUser_name().equals(oldUser.getUser_name())) {
                    unique = false;
                    break;
                }
            }
            if (unique) {
                FileOutputStream fos = new FileOutputStream("C:\\Users\\User\\IdeaProjects\\exercise2_taskmanager\\Task_Manager\\out\\artifacts\\Task_Manager_jar\\users");
                MyObjectOutputStream oos = new MyObjectOutputStream(f);
                userList.add(user);
                oos.writeObject(userList);
                oos.reset();
                oos.close();
                fos.close();
            } else System.out.println("This user_name already exists!");
        }
    }



    public static void showAllUsers() throws IOException, ClassNotFoundException {
        FileInputStream fis = new FileInputStream("C:\\Users\\User\\IdeaProjects\\exercise2_taskmanager\\Task_Manager\\out\\artifacts\\Task_Manager_jar\\users");
        ObjectInputStream ois = new ObjectInputStream(fis);
        userList = (ArrayList<User>) ois.readObject();
        for (User user : userList) {
            System.out.println(user);
        }
        ois.close();
        fis.close();
    }
}


