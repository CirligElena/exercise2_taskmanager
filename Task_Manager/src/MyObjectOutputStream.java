import java.io.*;

public class MyObjectOutputStream extends ObjectOutputStream {
    public static File file = null;

    public MyObjectOutputStream(File file) throws IOException {
        super(new FileOutputStream(file, true));
    }


    @Override
    public void writeStreamHeader() throws IOException {
        if (file != null) {
            if (file.length() == 0) {
                super.writeStreamHeader();
            } else {
                this.reset();
            }
        } else {
            super.writeStreamHeader();
        }
    }
}
