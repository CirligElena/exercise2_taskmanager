import java.io.*;
import java.util.ArrayList;

public class TaskCommands implements Serializable {
    static ArrayList<Task> taskList = new ArrayList<>();
    static File f = new File("C:\\Users\\User\\IdeaProjects\\exercise2_taskmanager\\Task_Manager\\out\\artifacts\\Task_Manager_jar\\tasks");

    public static void addTask(String insert_un, String task_title, String task_description) throws IOException, ClassNotFoundException {
        Task task = new Task();
        task.setInsert_un(insert_un);
        task.setTask_title(task_title);
        task.setTask_description(task_description);
        if (f.length() == 0) {
            FileOutputStream fos = new FileOutputStream("C:\\Users\\User\\IdeaProjects\\exercise2_taskmanager\\Task_Manager\\out\\artifacts\\Task_Manager_jar\\tasks");
            MyObjectOutputStream oos = new MyObjectOutputStream(f);
            taskList.add(task);
            oos.writeObject(taskList);
            oos.reset();
            oos.close();
            fos.close();
        } else {
            FileInputStream fis = new FileInputStream("C:\\Users\\User\\IdeaProjects\\exercise2_taskmanager\\Task_Manager\\out\\artifacts\\Task_Manager_jar\\tasks");
            ObjectInputStream ois = new ObjectInputStream(fis);
            taskList = (ArrayList<Task>) ois.readObject();
            ois.close();
            fis.close();
            FileOutputStream fos = new FileOutputStream("C:\\Users\\User\\IdeaProjects\\exercise2_taskmanager\\Task_Manager\\out\\artifacts\\Task_Manager_jar\\tasks");
            MyObjectOutputStream oos = new MyObjectOutputStream(f);
            taskList.add(task);
            oos.writeObject(taskList);
            oos.reset();
            oos.close();
            fos.close();
        }
    }

    public static void showTasks() throws IOException, ClassNotFoundException {
        FileInputStream fis = new FileInputStream("C:\\Users\\User\\IdeaProjects\\exercise2_taskmanager\\Task_Manager\\out\\artifacts\\Task_Manager_jar\\tasks");
        ObjectInputStream ois = new ObjectInputStream(fis);
        taskList = (ArrayList<Task>) ois.readObject();
        for (Task task : taskList) {
            System.out.println(task);
        }
        ois.close();
        fis.close();
    }
}
